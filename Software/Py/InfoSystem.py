#!/usr/bin/env python
from PIL import Image, ImageDraw, ImageFont
import time
import secret
from neopixel import *
import paho.mqtt.client as mqtt
import argparse
import os
import math
# LED strip configuration:
LED_COUNT = 16  # Number of LED pixels.
LED_PIN = 18  # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN            = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10  # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 10  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_ZigZac = True  # Leds gehen hin und zurück
LED_WIDTH = 32
LED_HEIGHT = 8
LED_PANEL = 4
LED_COUNT = LED_WIDTH * LED_HEIGHT * LED_PANEL  # Number of LED pixels.
MqttMsgData =''
# Variables for periodicity
LETTER_WIDTH = 6
DISPLAY_WIDTH = LED_WIDTH*LED_PANEL
DISPLAY_REMAINDER = (DISPLAY_WIDTH)%LETTER_WIDTH
DISPLAY_LETTERS = int( (DISPLAY_WIDTH - DISPLAY_REMAINDER) / LETTER_WIDTH )
TEXT_SPACE = 8 # Pixel between repetition of text (if longer than display)
SEPERATOR_WIDTH = 12 # introduces a seperator (two times the width of this value) where long text vanishes into, set to zero to dectivate.
# Fade at the separator to the following color
SEP_COLOR_r = 64
SEP_COLOR_g = 0
SEP_COLOR_b = 0
# INIT
currentDisplayText = 'Moin Moin. Viele Gruesse von den Hackerspaces im Norden'
displayText = currentDisplayText
textPos = 0
textPosWaitCycles = 3  # 50ms Takte
currentTextPosWaitCycles = 0
randomTime = 0
lastTime = 0
hackermanCount = 0
hackermanCountMax = 20

FrontColor_r = 0
FrontColor_g = 255
FrontColor_b = 12

BackColor_r = 64
BackColor_g = 0
BackColor_b = 0

TriggerTime = False #True
TriggerText = True #False
TriggerCountdown = False
TriggerTalkTimer = False
TriggerNewsFeed = False
TriggerProjekt = False
TriggerRandom = False
TriggerLamp = False
TriggerPower = False
TriggerHackerman = False
TriggerKalender =  False

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.connected_flag=True #set flag


def on_message(client, Data1, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    global MqttMsgExist
    global MqttMsgData
    MqttMsgData = str(msg.payload).replace('b\'', "")
    MqttMsgData = MqttMsgData.replace('\'', "")
    MqttMsgExist = True


def on_publish(client, mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(client, mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(client, mqttc, obj, level, string):
    print(string)


def render_text(textpos_x, text):
    # render_text(textpos_x, text) - render an image with the text starting at pixel textpos_x
    #
    #
    tC = (int(FrontColor_r), int(FrontColor_g), int(FrontColor_b)) # forground color tuple
    bC = (int(BackColor_r), int(BackColor_g), int(BackColor_b)) # background color tuple
    fnt = ImageFont.truetype('/home/pi/InfoSystem/Software/Py/pixelmix.ttf', 8)
    img = Image.new('RGB', (LED_WIDTH * LED_PANEL, LED_HEIGHT), bC)
    tL = len(text) # Text length in letters
    tLPx, tLPy = fnt.getsize(text)
    # get a drawing context
    d = ImageDraw.Draw(img)
    # Periodicity check #1 small text
    if tLPx < DISPLAY_WIDTH: # text fits display
        if textpos_x + tLPx > DISPLAY_WIDTH: #there's at least one pixel out -> add in front - keep remainder
            d.text((0 - textpos_x, 0), text, font=fnt, fill=tC)
        if textpos_x < tLPx: # There are still pixel that are before the beginning 
            d.text((0 - textpos_x, 0), text, font=fnt, fill=tC)
    else: # text too long for display
        d.text(( DISPLAY_WIDTH - tLPx - TEXT_SPACE - textpos_x, 0), text, font=fnt, fill=tC)
        d.text(( DISPLAY_WIDTH + tLPx + TEXT_SPACE - textpos_x, 0), text, font=fnt, fill=tC)
    # draw original text
    d.text(( DISPLAY_WIDTH - textpos_x, 0), text, font=fnt, fill=tC)
    # create separator if activated (longh text only)
    if (SEPERATOR_WIDTH > 0) and (tLPx > DISPLAY_WIDTH):
        sC = (SEP_COLOR_r,SEP_COLOR_g,SEP_COLOR_b)
        for i in range(1,SEPERATOR_WIDTH+1):
            dv = (SEPERATOR_WIDTH+1)
            fr = i-1
            fr2 = (dv-fr)/dv
            fr = fr/dv
            for j in range(0,LED_HEIGHT): # pixelwise manbual blending
                newC = tuple( int( fr2*p + fr*q ) for p , q in zip(sC, img.getpixel( (i-1,j))) )
                img.putpixel( (i-1,j), newC )
                newC = tuple( int( fr2*p + fr*q ) for p , q in zip(sC, img.getpixel( (DISPLAY_WIDTH-i,j) )) )
                img.putpixel( (DISPLAY_WIDTH-i,j), newC )
    return img

def fill_stripe_ram(panelAnzahl, width, height, BMP):
    # print('fillStripeRam')

    for PanelNr in range(panelAnzahl):
        for row in range(height):
            for column in range(width):
                if LED_ZigZac and ((row + 1) & 1):
                    pixelNr = (PanelNr * width * height) + column + (row * width)
                else:
                    pixelNr = (PanelNr * width * height) + (row * width) + (width - column) - 1
                if not LED_ZigZac:
                    pixelNr = (PanelNr * width * height) + column + (row * width)

                # print(PanelNr, column, row)
                # print('Farbe: %x', pixelNr)
                r, g, b = BMP.getpixel(((column + PanelNr * width), row))
                # print(r, g, b)
                strip.setPixelColor(pixelNr, Color(g, r, b))


def clear_all_and_set_trigger():
    global TriggerTime
    global TriggerText
    global TriggerCountdown
    global TriggerTalkTimer
    global TriggerNewsFeed
    global TriggerProjekt
    global randomTime
    global TriggerLamp
    global TriggerHackerman
    global TriggerKalender
    TriggerTime = False
    TriggerText = False
    TriggerCountdown = False
    TriggerTalkTimer = False
    TriggerNewsFeed = False
    TriggerProjekt = False
    randomTime = 0
    TriggerLamp = False
    TriggerKalender =False
    return True


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()
    hostname = secret.MqttServer #example
    response = 1
    while response != 0:
        global response
        response = os.system("ping -c 1 " + hostname)

        #and then check the response...
        if response != 0:
            print(hostname, 'is Down!…')
    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()
    mqtt.Client.connected_flag=False#create flag in class
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    
    client.connect(secret.MqttServer, 1883, 60)

    
    client.subscribe("InfoSystem", 0)
    client.loop_start()
    
    MqttMsgExist = False
    
    print('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:
        aktBMP = Image.new('RGB', (LED_WIDTH * LED_PANEL, LED_HEIGHT),
            (int(BackColor_r), int(BackColor_g), int(BackColor_b)))
        while True:
            # ----------------------------------------------------------------------------
            # Mqtt message  and variable settings
            # ----------------------------------------------------------------------------
            if MqttMsgExist:
                #print(MqttMsgData)
                key, value = MqttMsgData.split(':')
                #print(key)
                #print(value)
                # ---------------------------------------------------------------
                if key == 'speed':
                    textPosWaitCycles = int(value)
                    print('TextWaitCycles: ', value)
                # ---------------------------------------------------------------
                if key == 'Position':
                    textPos = int(value)
                    print('TextPos: ', value)
                # ---------------------------------------------------------------
                if key == 'FrontColor':  # set Front Color of the LED
                    value = value.replace("rgb(", "")
                    value = value.replace(")", "")
                    FrontColor_r, FrontColor_g, FrontColor_b = value.split(",")
                    print('FrontColor: ', FrontColor_r, FrontColor_g, FrontColor_b)
                # ---------------------------------------------------------------
                if key == 'BackColor':  # set back Color of the LED
                    value = value.replace("rgb(", "")
                    value = value.replace(")", "")
                    BackColor_r, BackColor_g, BackColor_b = value.split(",")
                    print('BackColor: ', BackColor_r, BackColor_g, BackColor_b)
                # ---------------------------------------------------------------
                if key == 'TimeTrigger':
                    TriggerTime = clear_all_and_set_trigger()
                    print('TimeTriggerSet')
                    print(TriggerTime)
                # ---------------------------------------------------------------
                if key == 'TextTrigger':
                    TriggerText = clear_all_and_set_trigger()
                    print('TextTriggerSet')
                # ---------------------------------------------------------------
                if key == 'CountdownTrigger':
                    TriggerCountdown = clear_all_and_set_trigger()
                    print('CountdownTriggerSet')
                # ---------------------------------------------------------------
                if key == 'TalkTimerTrigger':
                    TriggerTalkTimer = clear_all_and_set_trigger()
                    print('TalkTimeTriggerSet')
                # ---------------------------------------------------------------
                if key == 'NewsFeedTrigger':
                    TriggerNewsFeed = clear_all_and_set_trigger()
                    print('NewsFeedTriggerSet')
                # ---------------------------------------------------------------
                if key == 'ProjektTrigger':
                    TriggerProjekt = clear_all_and_set_trigger()
                    print('ProjektTriggerSet')
                # ---------------------------------------------------------------
                if key == "LampTrigger":
                    TriggerLamp = clear_all_and_set_trigger()
                    print('LampTriggerSet')
                # ---------------------------------------------------------------
                if key == 'RandomTime':
                    clear_all_and_set_trigger()
                    randomTime = int(value)
                    print('RandomTime:' + str(randomTime))
                # ---------------------------------------------------------------
                if key == "DisplayText":
                    displayText = value
                    print(displayText)
                if key == "DisplayKalender":
                    displayText = value
                    print(displayText)
                if key == "HackermanTrigger":
                    TriggerHackerman = True
                    hackermanCount = 0
                    hackermanCountMax = int(value)

                    print('HackermanTrigger')
                # ---------------------------------------------------------------
                if key == "Brightness":
                    LED_BRIGHTNESS = int(value)
                    print('Brightness: ', value)
                    strip.setBrightness(LED_BRIGHTNESS)
                # ---------------------------------------------------------------
                if key == "PowerTrigger":
                    if value == "false":
                        print('Brightness: ', 0)
                        strip.setBrightness(0)
                        strip.show
                        strip.show
                        os.system("sudo shutdown -h now")
                # ---------------------------------------------------------------


                MqttMsgExist = False

            # ----------------------------------------------------------------------------
            # Function settings
            # ----------------------------------------------------------------------------

            # ---------------------------------------------------------------
            #print(TriggerTime)
            if TriggerTime:
                currentDisplayText = time.strftime("%d.%m.%Y %H:%M:%S")
            # ---------------------------------------------------------------
            if TriggerText:
                currentDisplayText = displayText
            # ---------------------------------------------------------------
            if TriggerHackerman:

                currentDisplayText = "HACKERMAN HACKERMAN"
                textPos = 128
                textPosWaitCycles = 0
                strip.setBrightness(100)
                if hackermanCount < hackermanCountMax:
                    if (hackermanCount & 2) == 2:
                        FrontColor_r = 0
                        FrontColor_b = 0
                        FrontColor_g = 255
                        BackColor_r = 0
                        BackColor_b = 0
                        BackColor_g = 0
                    else:
                        FrontColor_r = 0
                        FrontColor_b = 0
                        FrontColor_g = 0
                        BackColor_r = 0
                        BackColor_b = 0
                        BackColor_g = 255
                hackermanCount += 1
                if hackermanCount >= hackermanCountMax:
                    currentDisplayText = backupText
                    textPos = backupPos
                    textPosWaitCycles = backupSpeed
                    FrontColor_r = backupFC_r
                    FrontColor_g = backupFC_g
                    FrontColor_b = backupFC_b
                    BackColor_r = backupBC_r
                    BackColor_g = backupBC_g
                    BackColor_b = backupBC_b
                    TriggerTime = bacTriggerTime
                    TriggerText = bacTriggerText
                    TriggerCountdown = bacTriggerCountdown
                    TriggerTalkTimer = bacTriggerTalkTimer
                    TriggerNewsFeed = bacTriggerNewsFeed
                    TriggerProjekt = bacTriggerProjekt
                    randomTime = bacrandomTime
                    TriggerLamp = bacTriggerLamp
                    TriggerHackerman = False
                    strip.setBrightness(backupBright)

            else:
                backupBright = strip.getBrightness()
                backupText = currentDisplayText
                backupPos = textPos
                backupSpeed = textPosWaitCycles
                backupFC_r = FrontColor_r
                backupFC_g = FrontColor_g
                backupFC_b = FrontColor_b
                backupBC_r = BackColor_r
                backupBC_g = BackColor_g
                backupBC_b = BackColor_b
                bacTriggerTime = TriggerTime
                bacTriggerText = TriggerText
                bacTriggerCountdown = TriggerCountdown
                bacTriggerTalkTimer = TriggerTalkTimer
                bacTriggerNewsFeed = TriggerNewsFeed
                bacTriggerProjekt = TriggerProjekt
                bacrandomTime = randomTime
                bacTriggerLamp = TriggerLamp
            # ---------------------------------------------------------------
            # set BMP to Background Color
            # aktBMP = Image.new('RGB', (LED_WIDTH * LED_PANEL, LED_HEIGHT),
            #                (int(BackColor_r), int(BackColor_g), int(BackColor_b)))
            # ---------------------------------------------------------------
            # move text
            currentTextPosWaitCycles += 1
            if currentTextPosWaitCycles >= textPosWaitCycles:
                currentTextPosWaitCycles = 0
                # update bitmap and put onto lighthouse - includes periodicity
                aktBMP = render_text(textPos,currentDisplayText)
                # shift if the really have text and after one waitcycle
                if len(currentDisplayText) != 0 and textPosWaitCycles != 0:
                    # shift until the last letter has vanished (maybe just LED_WIDTH*LED_PANEL is enough?)
                    # comput shift amount
                    if len(currentDisplayText) <= DISPLAY_LETTERS: # text fits display
                        shiftmodulo = DISPLAY_WIDTH
                    else: # if it does not fit, shift one letter more than length
                        fnt = ImageFont.truetype('/home/pi/InfoSystem/Software/Py/pixelmix.ttf', 8)
                        tLPx, tLPy = fnt.getsize(currentDisplayText)
                        shiftmodulo = tLPx+TEXT_SPACE
                    textPos = (textPos+1) % ( shiftmodulo )
            # ---------------------------------------------------------------
            # copy Bitmap to RGB Matrix ram
            fill_stripe_ram(LED_PANEL, LED_WIDTH, LED_HEIGHT, aktBMP)
             # ---------------------------------------------------------------
            # wait for minimum 50ns
            while (int(round(time.time() * 1000)) - lastTime) < 0.05:  # wait until 50ms are done
                pass
            lastTime = int(round(time.time() * 1000))
            # ---------------------------------------------------------------
            # Write RGB Matrix Hardware
            strip.show()

            # print(lastTime)

    except KeyboardInterrupt:
        client.loop_stop()
        if args.clear:
            print('ende')
